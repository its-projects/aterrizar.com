/// <reference path='../typings/angularjs/angular.d.ts' />
/// <reference path='../typings/lodash/lodash.d.ts' />

angular.module('Aterrizar', ['ngMaterial', 'ngRoute'])

    .config(function($mdThemingProvider, $routeProvider) {

        $mdThemingProvider.theme('default')
            .primaryPalette('indigo')
            .accentPalette('deep-orange');

        $routeProvider
            .when('/index', {
                templateUrl: '../app.html'
            })
            .otherwise({
                redirectTo: '/index'
            });
    })

    .controller('MainCtrl', function($scope) {
        $scope.deal = {
            id: '1234',
            name: 'The great hotel',
            services: ['WiFi', 'Desayuno', 'Calefacción', 'Bar', 'Asador', 'Piscina'],
            briefDescription: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
        };

        $scope.suggestions = [{
            id: '1',
            name: 'Playa del Carmen',
            image: 'https://zonaturistica1.scdn5.secure.raxcdn.com/files/atractivos/517/A517.jpg',
            briefDescription: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
        },
        {
            id: '2',
            name: 'Punta del este',
            image: 'http://www.puntadeleste.com/artworks/informacion/items/punta_del_este/La_Mano_de_Punta_del_Este_toma3_big.jpg',
            briefDescription: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
        },
        {
            id: '3',
            name: 'Buenos aires',
            image: 'http://images.friendlyrentals.com/FR_imgs/blog/PrImg_3079.jpg',
            briefDescription: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
        }];
    })

    .directive('forcedWidth', function() {
        return {
            restrict: 'A',
            link: function (scope, element, attrs : any) {
                element.css('min-width', attrs.forcedWidth + "px");
                element.css('max-width', attrs.forcedWidth + "px");
            }
        };
    });
